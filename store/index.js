import Vuex from "vuex"
import axios from "axios"

let BASE_API = process.env.BASE_API || 'https://shiba-api.cii3.net/'

const Book = () => {
    return new Vuex.Store({
        state: {
            loadBookData: [],
            cartData: []
        },
        mutations: {
            setBooksState(state, books) {
                state.loadBookData = books
            },
            setCart(state, data) {
                const bookIndex = state.cartData.findIndex(find => find.id === data.id)
                if (bookIndex !== -1) {
                    data.qty += state.cartData[bookIndex].qty
                    state.cartData.splice(bookIndex, 1, data)
                } else {
                    state.cartData.push(data)
                }
            },
            setCartRemove(state) {
                state.cartData = []
            },
            setCartUpdate(state, data) {
                const bookIndex = state.cartData.findIndex(find => find.id === data.id)
                if (bookIndex !== -1) {
                    if (data.qty == 0) {
                        state.cartData.splice(bookIndex, 1)
                    } else {
                        state.cartData.splice(bookIndex, 1, data)
                    }
                }
            },
            editBook(state, data) {
                const bookIndex = state.loadBookData.findIndex(find => find._id === data._id)
                if (bookIndex !== -1) {
                    state.loadBookData.splice(bookIndex, 1, data)
                }
            },
            addNewBook(state, data) {
                state.loadBookData.reverse().push(data)
                state.loadBookData.reverse()
            },
            removeBook(state, data) {
                const bookIndex = state.loadBookData.findIndex(find => find._id === data)
                if (bookIndex !== -1) {
                    state.loadBookData.splice(bookIndex, 1)
                }
            },
        },
        actions: {
            async nuxtServerInit(vuexContext, context) {
                try {
                    const res = await axios.get(`${BASE_API}api/book?sort=desc`);
                    vuexContext.commit("setBooksState", res.data);
                }
                catch (e) {
                    return context.error(e);
                }
            },
            addToCart(vuexContext, data) {
                vuexContext.commit("setCart", data)
            },
            removeAllCart(vuexContext) {
                vuexContext.commit("setCartRemove")
            },
            updateCart(vuexContext, data) {
                vuexContext.commit("setCartUpdate", data)
            },
            async addNewBook(vuexContext, data) {
                const res = await axios({
                    method: 'put',
                    url: `${BASE_API}api/book`,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify(data)
                })
                vuexContext.commit("addNewBook", { ...res.data[0] })
            },
            async editBook(vuexContext, data) {
                const res = await axios({
                    method: 'post',
                    url: `${BASE_API}api/book/${data._id}`,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        id: data.id,
                        cover: data.cover,
                        title: data.title,
                        price: data.price
                    })
                })
                vuexContext.commit("editBook", { ...data })
            },
            async removeBook(vuexContext, data) {

                await axios({
                    method: 'delete',
                    url: `${BASE_API}api/book/${data._id}`,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }).then(() => {
                    vuexContext.commit("removeBook", data._id)
                })

                return true
            }
        },
        getters: {
            getAllBooks(state) {
                return state.loadBookData
            },
            getCartData(state) {
                return state.cartData
            }
        }
    });
};
export default Book