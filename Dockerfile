FROM node:12.16.3-slim

RUN ln -fs /usr/share/zoneinfo/Asia/Bangkok /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

RUN mkdir -p /app

COPY / /app

WORKDIR /app

RUN npm install

RUN npm run build

ENV HOST 0.0.0.0

CMD [ "npm", "run", "start"]