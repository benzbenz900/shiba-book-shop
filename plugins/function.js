export default (context, inject) => {

    // ตำนวนส่วนลดจาก เปอร์เซ็น
    const calsPercentageDiscount = (price, discount) => {
        return parseInt(price) * parseInt(discount) / 100
    }

    // หากราคารวมและ เช็คว่าเข้าเงื่อนไข ของจำนวนหนังสือที่จะลดราคา ต่อเนื่องหรือไม
    const sumTotalPrice = (cartItem) => {
        // เก็บส่วนลดที่คำนวนแล้ว
        let TotalDiscount = 0

        // เก็บราคารวมก่อนลด
        let TotalPrice = 0

        // ส่วนลดเมือชื้อหนังสือเล่มเดียวกัน เก็บใน Array DiscountStep
        let DiscountStep = [
            0, //ชื่อ 1 เล่น
            10, //ชื่อ 2 เล่น
            11, //ชื่อ 3 เล่น 
            12, //ชื่อ 4 เล่น
            13, //ชื่อ 5 เล่น
            14, //ชื่อ 6 เล่น
            15 //ชื่อ 7 เล่น
        ]

        // คำนวน ราคา และ ส่วนลด
        cartItem.map(eliment => {
            TotalPrice += eliment.price * eliment.qty
            TotalDiscount += calsPercentageDiscount(
                // เช็คว่า ชื้อถึง 7 เล่นหรือยัง
                (eliment.qty >= 7) ?
                    // ถ้าถึง 7 เล่นแล้ว ให้คำนวนราคาจาก 7 เล่นแทน
                    eliment.price * 7 :
                    // ถ้ายังไม่ถึง ให้คำนวนจากค่าที่ส่งมา
                    eliment.price * eliment.qty
                ,
                // เช็คว่า ชื้อถึง 7 เล่นหรือยัง
                (eliment.qty >= 7) ?
                    // ถ้าถึง 7 เล่นแล้ว ให้ให้ใช้ ส่วนลดจาก Array DiscountStep Index ที่ 6
                    DiscountStep[6] :
                    // ถ้ายังไม่ถึง ให้คำนวนจากค่าที่ส่งมา แล้ว ลบด้วย 1
                    DiscountStep[eliment.qty - 1]
            )
        })
        // คืนค่า ราคา และส่วงนลดที่ได้ ออกไป
        return { net: TotalPrice - TotalDiscount, discount: TotalDiscount }
    }

    // ส่งฟังชั่นออกไป ให้สามารถเรียกใช้งานที่ไหน ของโปรเจ็คก็ได้
    inject('sumTotalPrice', sumTotalPrice)
}