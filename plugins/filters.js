import Vue from 'vue'
import VueHtmlToPaper from 'vue-html-to-paper';
const numeral = require('numeral')

const options = {
    name: '_blank',
    specs: [
        'fullscreen=yes',
        'titlebar=yes',
        'scrollbars=yes'
    ],
    styles: [
        'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'
    ]
}

Vue.use(VueHtmlToPaper, options);

// สร้าง pipe สำหรับจัดรูปแบบ ราคา ให้สามารถเรียกใช้งานที่ไหน ของโปรเจ็คก็ได้
Vue.filter('numberPrice', value => {
    return '฿' + numeral(value).format('0,0.00')
})