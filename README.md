# Shiba Book Shop

## หากเขียนไม่ถูกหลักของ NuxtJS อย่างไรต้องขออภัยด้วยนะครับ พึ่งศึกษา หลังจากได้ไฟล์สำหรับ Test งานมาครับ

สามารถทดสอบได้ที่ Heroku https://shibaposshop.herokuapp.com/

Test On Heroko https://shibaposshop.herokuapp.com/

## ภาพหน้าจอ

หน้าแรกแสดงหนังสือทั้งหมด จาก API ที่กำหนดไว้ให้ https://api.jsonbin.io/b/5e69b564d2622e7011565547
![หน้าแรกแสดงหนังสือทั้งหมด](https://www.awesomescreenshot.com/upload/1006579/1038315/35b7a887-5b59-4dc9-4f49-23f354270727.png)

ใส้สินค้าในตะกร้า
![ใส้สินค้าในตะกร้า](https://www.awesomescreenshot.com/upload/1006579/1038315/4d4be386-41e7-4bbf-711e-a0cc7eb96a41.png)

ชำระเงินด้วยเงินสด
![ชำระเงินด้วยเงินสด](https://www.awesomescreenshot.com/upload/1006579/1038315/759ae3db-747d-4f35-668a-a85c2b5f3a1f.png)

ใส่จำนวนเงินขาด พร้อมแจ้งจำนวนเงินที่ขาด
![ใส่จำนวนเงินขาด พร้อมแจ้งจำนวนเงินที่ขาด](https://www.awesomescreenshot.com/upload/1006579/1038315/23f8abee-47a6-468a-40ee-b8d5dd404a21.png)

ชำระเงินสำเร็จ
![ชำระเงินสำเร็จ](https://www.awesomescreenshot.com/upload/1006579/1038315/e03476d3-eaf2-45cb-4bbe-7066116b7e61.png)

พิมพ์สลิป
![พิมพ์สลิป](https://www.awesomescreenshot.com/upload/1006579/1038315/8e10fb4a-2614-44a5-64a3-363ce31dacf8.png)

รายการหนังสือในร้าน
![รายการหนังสือในร้าน](https://www.awesomescreenshot.com/upload/1006579/1038315/38734cbe-9979-4f0a-7621-97aa1f4b831b.png)

เพิ่มหนังสือใหม่
![เพิ่มหนังสือใหม่](https://www.awesomescreenshot.com/upload/1006579/1038315/db5d0c46-af48-464e-77df-33bfad560fed.png)

แก้ไขหนังสือ
![แก้ไขหนังสือ](https://www.awesomescreenshot.com/upload/1006579/1038315/21f1656d-1a5c-427e-5108-a11a626fae4f.png)


## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Docker Setup

```bash
# docker build image
$ docker build -t shiba .

# serve with hot reload at localhost:3000
$ docker run -e BASE_API="https://shiba-api.cii3.net/" -p 3000:3000 -d shiba
```
## Docker Compose docker-compose.yml

```bash
version: '3.3'
services:
    shiba:
        environment:
            - 'BASE_API=https://shiba-api.cii3.net/'
        ports:
            - '3000:3000'
        image: shiba
```

## Docker Compose Up

```bash
$ docker-compose up -d
```

## Deploy on Heroku
```bash
$ heroku login
$ heroku git:remote -a shibaposshop #your git heruku name https://git.heroku.com/shibaposshop.git
$ heroku config:set HOST=0.0.0.0
$ heroku config:set NODE_ENV=production
$ heroku config:set BASE_API=https://shiba-api.cii3.net/
$ git add .
$ git commit -am "make it better"
$ git push heroku master
```